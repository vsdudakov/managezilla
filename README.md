Managezilla
=========
## Description
There is a task board for bugzilla.
Now it is readonly mode. You can not change data of tasks using this board (may be in the future).
Anyway any suggestions are welcome!


## First steps
Powered by django framework

1. install python-virtualenv (sudo apt-get install python-virtualenv && virtualenv your_virtual_env)
1. source your_virtual_env/bin/activate
1. install dependencies to virtualenv (pip install -r requirements.txt)
1. create settings.py file (cp managezilla/settings.py.tmpl managezilla/settings.py)
1. cusomize some parameters in your settings (managezilla/settings.py) (see Parameters section)
1. run django test server (python manage.py runserver)
1. create superuser (python manage.py createsuperuser)
1. sync sqlite db (python manage.py migrate)
1. go to 127.0.0.1:8000 in your browser


## Contact

[Vsevolod Dudakov](http://github.com/vsdudakov)(vsdudakov@gmail.com)

## Licence

Managezilla is available under the MIT licence. See the LICENCE file for more info.
