from django.conf.urls import  include, url
from django.contrib import admin


from main.views import LoginView, LogoutView, TaskBoardView, BacklogBoardView, ReportView


urlpatterns = (
    url(r'^$', BacklogBoardView.as_view(), name='backlog'),
    url(r'^board/$', TaskBoardView.as_view(), name='board'),
    url(r'^report/$', ReportView.as_view(), name='report'),

    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),

    url(r'^admin/', include(admin.site.urls)),
)
