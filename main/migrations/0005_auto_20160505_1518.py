# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-05 15:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20160505_1514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reporttask',
            name='time',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
