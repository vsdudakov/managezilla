from django import forms
from django.conf import settings

from main.helpers import (
    get_choices,
    get_current_milestone,
    get_current_year,
    get_current_week
)


class BootstrapForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(BootstrapForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['style'] = 'width: 100%;'


class BootstrapModelForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BootstrapModelForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['style'] = 'width: 100%;'


class LoginForm(BootstrapForm):
    username = forms.CharField(max_length=255)
    password = forms.CharField(max_length=255, widget=forms.PasswordInput())


class TaskFilterForm(BootstrapForm):
    groups = forms.MultipleChoiceField(choices=get_choices(settings.TASK_GROUPS.keys()), required=False)
    users = forms.MultipleChoiceField(choices=get_choices(settings.TASK_USERS, handler=lambda x:x.split('@')[0]), required=False)
    milestones = forms.MultipleChoiceField(choices=get_choices(settings.TASK_MILESTONES), required=False)
    statuses = forms.MultipleChoiceField(choices=get_choices(settings.TASK_STATUSES), required=False)
    types = forms.MultipleChoiceField(choices=get_choices(settings.TASK_TYPES), required=False)
    priorities = forms.MultipleChoiceField(choices=get_choices(settings.TASK_PRIORITIES), required=False)
    severities = forms.MultipleChoiceField(choices=get_choices(settings.TASK_SEVERITIES), required=False)

    def __init__(self, *args, **kwargs):
        super(TaskFilterForm, self).__init__(*args, **kwargs)
        self.fields['groups'].initial = [settings.TASK_GROUPS.keys()[0]]
        self.fields['milestones'].initial = [get_current_milestone()]
        self.fields['statuses'].initial = [settings.TASK_NOT_RESOLVED]
        self.fields['types'].initial = [settings.ALL_ITEMS]
        self.fields['priorities'].initial = [settings.ALL_ITEMS]
        self.fields['severities'].initial = [settings.ALL_ITEMS]


class BacklogFilterForm(BootstrapForm):
    groups = forms.MultipleChoiceField(choices=get_choices(settings.TASK_GROUPS.keys()), required=False)
    users = forms.MultipleChoiceField(choices=get_choices(settings.TASK_USERS, handler=lambda x:x.split('@')[0]), required=False)
    milestones = forms.MultipleChoiceField(choices=get_choices(settings.TASK_MILESTONES), required=False)
    backlog_users = forms.MultipleChoiceField(choices=get_choices(settings.BACKLOG_USERS, handler=lambda x:x.split('@')[0]), required=False)

    def __init__(self, *args, **kwargs):
        super(BacklogFilterForm, self).__init__(*args, **kwargs)
        self.fields['groups'].initial = [settings.TASK_GROUPS.keys()[0]]
        self.fields['milestones'].initial = [settings.ALL_ITEMS]
        self.fields['backlog_users'].initial = [settings.BACKLOG_USERS[0]]


class ReportFilterForm(BootstrapForm):
    groups = forms.MultipleChoiceField(choices=get_choices(settings.TASK_GROUPS.keys()), required=False)
    users = forms.MultipleChoiceField(choices=get_choices(settings.TASK_USERS, handler=lambda x:x.split('@')[0]), required=False)

    def __init__(self, *args, **kwargs):
        super(ReportFilterForm, self).__init__(*args, **kwargs)
