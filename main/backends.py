#-*- encoding: utf-8 -*-
import xmlrpclib

from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model


class BzBackend(ModelBackend):

    def __init__(self, *args, **kwargs):
        super(BzBackend, self).__init__(*args, **kwargs)
        self.bz = xmlrpclib.ServerProxy(settings.BUGZILLA_URL + 'xmlrpc.cgi')

    def authenticate(self, username=None, password=None, **kwargs):
        if username == 'admin':
            return super(BzBackend, self).authenticate(username=username, password=password, **kwargs)
        UserModel = get_user_model()
        if username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD)

        kwargs = {}
        kwargs['login'] = username
        kwargs['password'] = password
        try:
            token = self.bz.User.login(kwargs).get('token')
            if not token:
                return
            user, _ = UserModel.objects.get_or_create(username=username)
            user.set_password(password)
            user.save()
            user._token = token
            return user
        except xmlrpclib.Fault:
            return

    def logout(self, token):
        self.bz.User.logout({'Bugzilla_token': token})
