import xmlrpclib
import datetime
import random

from django.conf import settings
from django.db import models


from main.helpers import (
    get_choices,
    short_str,
    get_current_year,
    get_current_week,
    get_start_current_week
)

# https://www.bugzilla.org/docs/4.4/en/html/api/Bugzilla/WebService/Bug.html#get
class TaskManager(models.Manager):

    def _get_bugzilla_tasks(
        self,
        bz,
        token,
        last_change_time=None,
        ids=(),
        users=(),
        statuses=(),
        types=(),
        milestones=(),
        priorities=(),
        severities=(),
        is_load_additional_info=False
        ):
        kwargs = dict()
        kwargs['Bugzilla_token'] = token

        search_kwargs = {}
        search_kwargs.update(kwargs)
        if ids:
            search_kwargs['id'] = ids
        else:
            if last_change_time:
                search_kwargs['last_change_time'] = last_change_time

            if settings.ALL_ITEMS in statuses:
                search_kwargs['status'] = settings.TASK_STATUSES
            elif settings.TASK_NOT_RESOLVED in statuses:
                search_kwargs['status'] = tuple(set(settings.TASK_STATUSES) - set(settings.TASK_CLOSE_STATUSES) - set([settings.ALL_ITEMS]))
            else:
                search_kwargs['status'] = statuses

            if settings.ALL_ITEMS not in types:
                search_kwargs['cf_tickettype'] = types
            else:
                search_kwargs['cf_tickettype'] = settings.TASK_TYPES

            if settings.ALL_ITEMS not in users:
                search_kwargs['assigned_to'] = users
            else:
                search_kwargs['assigned_to'] = settings.TASK_USERS

            if settings.ALL_ITEMS not in milestones:
                search_kwargs['target_milestone'] = milestones

            if settings.ALL_ITEMS not in priorities:
                search_kwargs['priority'] = priorities

            if settings.ALL_ITEMS not in severities:
                search_kwargs['severity'] = severities

        bugs = bz.Bug.search(search_kwargs)['bugs']
        if is_load_additional_info:
            history_kwargs = {}
            history_kwargs.update(kwargs)
            history_kwargs['ids'] = [bug['id'] for bug in bugs]
            histories = bz.Bug.history(history_kwargs)['bugs']
            histories = {history['id']: history['history'] for history in histories}
            for bug in bugs:
                bug['history'] = histories.get(bug['id'], '')

            comments_kwargs = {}
            comments_kwargs.update(kwargs)
            comments_kwargs['ids'] = [bug['id'] for bug in bugs]
            comments = bz.Bug.comments(comments_kwargs)['bugs']
            for bug in bugs:
                bug['comments'] = comments.get(str(bug['id']), '')
        return bugs

    def get_tasks(
        self,
        bz,
        token,
        last_change_time=None,
        ids=(),
        groups=(),
        users=(),
        statuses=(),
        types=(),
        milestones=(),
        priorities=(),
        severities=(),
        backlog_users=(),
        is_load_additional_info=False
        ):
        for group in groups:
            users += settings.TASK_GROUPS[group]
        bg_tasks = self._get_bugzilla_tasks(
            bz,
            token,
            last_change_time=last_change_time,
            ids=ids,
            users=users,
            statuses=statuses,
            types=types,
            milestones=milestones,
            priorities=priorities,
            severities=severities,
            is_load_additional_info=is_load_additional_info
            )
        tasks = []
        for bg_task in bg_tasks:
            task = self.model(
                id=bg_task['id'],
                summary=bg_task['summary'],
                assigned_to=bg_task['assigned_to'],
                creator=bg_task['creator'],
                priority=bg_task['priority'],
                severity=bg_task['severity'],
                cf_tickettype=bg_task['cf_tickettype'],
                status=bg_task['status'],
                resolution=bg_task['resolution'],
                product=bg_task['product'],
                component=bg_task['component'],
                cf_customer=bg_task['cf_customer'],
                blocks=bg_task['blocks'],
                depends_on=bg_task['depends_on'],
                target_milestone=bg_task['target_milestone'],
                creation_time=bg_task['creation_time'],
                last_change_time=bg_task['last_change_time'],
                actual_time=bg_task.get('actual_time', 0),
                estimated_time=bg_task.get('estimated_time', 0),
                remaining_time=bg_task.get('remaining_time', 0),
            )
            task.history = bg_task.get('history')
            task.comments = bg_task.get('comments')
            tasks.append(task)
        return tasks


    """
    [{'changes': [{'removed': '', 'field_name': 'deadline', 'added': '2016-02-28'}, {'removed': 'backend@revelsystems.com', 'added': 'eliza@revelsystems.com', 'field_name': 'assigned_to'}, {'removed': 'Undefined', 'added': 'P3-Medium', 'field_name': 'priority'}, {'removed': 'NEW', 'field_name': 'status', 'added': 'NEED DESIGN'}], 'who': 'cathy@revelsystems.com', 'when': <DateTime '20160104T21:42:55' at 10881ac20>}, {'when': <DateTime '20160105T17:43:15' at 10881add0>, 'who': 'svitlana.furs@revelsystems.com', 'changes': [{'removed': '', 'field_name': 'cc', 'added': 'svitlana.furs@revelsystems.com'}, {'removed': 'leonid@revelsystems.com', 'added': 'svitlana.furs@revelsystems.com', 'field_name': 'qa_contact'}]}, {'when': <DateTime '20160122T21:06:05' at 10881ae60>, 'who': 'cathy@revelsystems.com', 'changes': [{'removed': '', 'field_name': 'cf_spec', 'added': 'Move the Tax tab to the EMS.  This involves the following:\r\n1.  Implement the New UI\r\n2.  Implement EMS management of taxes\r\n3.  Put it on the new code base\r\n4.  Permission it\r\n\r\nFeel free to break it into substories, obviously.'}]}, {'changes': [{'removed': '2016-02-28', 'added': '2016-02-29', 'field_name': 'deadline'}], 'who': 'cathy@revelsystems.com', 'when': <DateTime '20160122T22:19:20' at 10881aef0>}, {'changes': [{'removed': 'NEED DESIGN', 'field_name': 'status', 'added': 'CONFIRMED'}, {'removed': 'Move the Tax tab to the EMS.  This involves the following:\r\n1.  Implement the New UI\r\n2.  Implement EMS management of taxes\r\n3.  Put it on the new code base\r\n4.  Permission it\r\n\r\nFeel free to break it into substories, obviously. ', 'added': 'Move the Tax tab to the EMS.  This involves the following:\r\n1.  Implement the New UI\r\n2.  Implement EMS management of taxes\r\n3.  Put it on the new code base\r\n4.  Permission it\r\n\r\nFeel free to break it into substories, obviously.\r\n\r\nWrite up is in comment 1.  If you would prefer to have this write up in a word document, let me know and I will happily do that.', 'field_name': 'cf_spec'}, {'removed': 'P3-Medium', 'added': 'P2-High', 'field_name': 'priority'}, {'removed': 'eliza@revelsystems.com', 'field_name': 'assigned_to', 'added': 'backend@revelsystems.com'}, {'removed': '', 'added': 'enterprise', 'field_name': 'keywords'}], 'who': 'cathy@revelsystems.com', 'when': <DateTime '20160129T22:02:25' at 10881af80>}, {'changes': [{'removed': '', 'added': 'eliza@revelsystems.com', 'field_name': 'cc'}], 'who': 'eliza@revelsystems.com', 'when': <DateTime '20160129T22:47:35' at 108822050>}, {'when': <DateTime '20160203T11:38:39' at 108822128>, 'who': 'dkulagin@revelsystems.com', 'changes': [{'removed': '', 'field_name': 'cc', 'added': 'dkulagin@revelsystems.com'}, {'removed': '---', 'added': '022916>2.15', 'field_name': 'target_milestone'}, {'removed': 'backend@revelsystems.com', 'field_name': 'assigned_to', 'added': 'platform-team@revelsystems.com'}]}, {'when': <DateTime '20160209T17:33:10' at 1088221b8>, 'who': 'igavrilov@revelsystems.com', 'changes': [{'removed': '', 'field_name': 'blocks', 'added': '37902'}]}, {'changes': [{'removed': '', 'field_name': 'blocks', 'added': '38046'}], 'who': 'igavrilov@revelsystems.com', 'when': <DateTime '20160211T15:16:07' at 108822248>}, {'changes': [{'removed': '', 'field_name': 'depends_on', 'added': '38104'}], 'who': 'dkulagin@revelsystems.com', 'when': <DateTime '20160212T06:59:35' at 1088222d8>}, {'changes': [{'removed': '', 'added': '38105', 'field_name': 'blocks'}], 'who': 'dkulagin@revelsystems.com', 'when': <DateTime '20160212T07:10:58' at 108822368>}, {'when': <DateTime '20160212T07:19:57' at 1088223f8>, 'who': 'dkulagin@revelsystems.com', 'changes': [{'removed': '', 'added': '38106', 'field_name': 'blocks'}]}, {'changes': [{'removed': '', 'field_name': 'cc', 'added': 'igavrilov@revelsystems.com'}], 'who': 'igavrilov@revelsystems.com', 'when': <DateTime '20160213T15:25:46' at 108822488>}, {'when': <DateTime '20160224T14:52:16' at 108822518>, 'who': 'igavrilov@revelsystems.com', 'changes': [{'removed': '', 'added': '38671', 'field_name': 'blocks'}]}, {'changes': [{'removed': '', 'added': '38775', 'field_name': 'blocks'}], 'who': 'nikolay.kurevin@revelsystems.com', 'when': <DateTime '20160225T13:06:31' at 1088225a8>}, {'changes': [{'removed': '022916>2.15', 'field_name': 'target_milestone', 'added': '032816>2.16'}], 'who': 'dkulagin@revelsystems.com', 'when': <DateTime '20160226T07:38:38' at 108822680>}, {'when': <DateTime '20160229T15:22:44' at 108822710>, 'who': 'dtoguzova@revelsystems.com', 'changes': [{'removed': '38775', 'field_name': 'blocks', 'added': ''}]}, {'when': <DateTime '20160301T12:47:53' at 1088227e8>, 'who': 'integration@revelsystems.com', 'changes': [{'removed': '032816>2.16', 'added': '022916>2.15', 'field_name': 'target_milestone'}, {'removed': '', 'field_name': 'cf_fixed_in', 'added': '022916>2.15'}]}, {'changes': [{'removed': '022916>2.15', 'added': '032816>2.16', 'field_name': 'target_milestone'}], 'who': 'dkulagin@revelsystems.com', 'when': <DateTime '20160301T12:59:08' at 108822878>}, {'when': <DateTime '20160301T13:22:00' at 108822950>, 'who': 'integration@revelsystems.com', 'changes': [{'removed': '032816>2.16', 'added': '022916>2.15', 'field_name': 'target_milestone'}]}, {'changes': [{'removed': '022916>2.15', 'added': '032816>2.16', 'field_name': 'target_milestone'}], 'who': 'dkulagin@revelsystems.com', 'when': <DateTime '20160301T13:28:23' at 1088229e0>}, {'changes': [{'removed': '', 'field_name': 'blocks', 'added': '39216'}], 'who': 'vdudakov@revelsystems.com', 'when': <DateTime '20160303T15:21:39' at 108822a70>}, {'when': <DateTime '20160325T22:54:40' at 108822b90>, 'who': 'annette.amaral@revelsystems.com', 'changes': [{'removed': '', 'added': 'annette.amaral@revelsystems.com', 'field_name': 'cc'}]}, {'changes': [{'removed': 'platform-team@revelsystems.com', 'added': 'vdudakov@revelsystems.com', 'field_name': 'assigned_to'}], 'who': 'dkulagin@revelsystems.com', 'when': <DateTime '20160327T18:33:23' at 108822c20>}, {'when': <DateTime '20160328T14:50:49' at 108822cf8>, 'who': 'vdudakov@revelsystems.com', 'changes': [{'removed': '', 'field_name': 'blocks', 'added': '40582, 40581, 40583'}]}, {'when': <DateTime '20160328T15:10:04' at 108822d88>, 'who': 'vdudakov@revelsystems.com', 'changes': [{'removed': 'CONFIRMED', 'field_name': 'status', 'added': 'ASSIGNED'}]}, {'when': <DateTime '20160328T15:30:39' at 108822e18>, 'who': 'vdudakov@revelsystems.com', 'changes': [{'removed': '', 'added': 'FIXED', 'field_name': 'resolution'}, {'removed': 'ASSIGNED', 'field_name': 'status', 'added': 'RESOLVED'}]}]
    """


class Task(models.Model):
    summary = models.CharField(max_length=255, null=True, blank=True)

    assigned_to = models.CharField(max_length=255, null=True, blank=True, choices=get_choices(settings.TASK_USERS))
    creator = models.CharField(max_length=255, null=True, blank=True, choices=get_choices(settings.TASK_USERS))

    priority = models.CharField(max_length=255, null=True, blank=True, choices=get_choices(settings.TASK_PRIORITIES))
    severity = models.CharField(max_length=255, null=True, blank=True, choices=get_choices(settings.TASK_SEVERITIES))

    cf_tickettype = models.CharField(max_length=255, null=True, blank=True, choices=get_choices(settings.TASK_TYPES))
    status = models.CharField(max_length=255, null=True, blank=True, choices=get_choices(settings.TASK_STATUSES))
    resolution = models.CharField(max_length=255, null=True, blank=True)

    product = models.CharField(max_length=255, null=True, blank=True)
    component = models.CharField(max_length=255, null=True, blank=True)

    cf_customer = models.CharField(max_length=255, null=True, blank=True)

    blocks = models.CharField(max_length=255, null=True, blank=True)
    depends_on = models.CharField(max_length=255, null=True, blank=True)

    target_milestone = models.CharField(max_length=255, null=True, blank=True, choices=get_choices(settings.TASK_MILESTONES))

    creation_time = models.DateTimeField(null=True, blank=True)
    last_change_time = models.DateTimeField(null=True, blank=True)

    actual_time = models.FloatField(default=0.0, null=True, blank=True)
    estimated_time = models.FloatField(default=0.0, null=True, blank=True)
    remaining_time = models.FloatField(default=0.0, null=True, blank=True)

    history = None
    comments = None

    objects = TaskManager()

    @property
    def title(self):
        return self.summary

    @property
    def task_id(self):
        return self.id

    @property
    def task_status(self):
        return self.status

    @property
    def task_type(self):
        return self.cf_tickettype

    @property
    def task_resolution(self):
        return self.resolution

    @property
    def customer(self):
        return self.cf_customer

    @property
    def milestone(self):
        return self.target_milestone

    @property
    def area(self):
        return self.product

    def get_spent_hours(self):
        return int(self.actual_time)

    def get_tracker_url(self):
        return settings.BUGZILLA_URL + u'show_bug.cgi?id=%s' % self.task_id

    def is_closed(self):
        return self.task_status in settings.TASK_CLOSE_STATUSES

    def get_short_summary(self):
        return short_str(self.summary, 60)

    def get_short_assigned_to(self):
        return short_str(self.assigned_to, 20, separator='@')

    def get_short_milestone(self):
        return short_str(self.milestone, 6, separator='>', item=1)

    def get_year(self):
        return get_current_year()

    def get_week(self):
        return get_current_week()

    def get_u(self):
        start_current_week = get_start_current_week()
        if self.history is None or self.comments is None:
            return u'U' if self.get_spent_hours() else u''
        return u'U'

    def get_progress_status(self):
        return u'Done' if self.is_closed() else u'In progress...'

    def get_feature(self):
        return u'F' if self.task_type in settings.TASK_FEATURE_TYPES else u''

    def get_color(self):
        if self.task_type in settings.TASK_FEATURE_TYPES:
            return 'red'
        if self.task_type in ('Enhancement',):
            return 'blue'
        return 'gray'

    def is_customer(self):
        return self.customer

