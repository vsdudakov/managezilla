from django import template
from django.conf import settings

register = template.Library()


@register.filter(name='filter_by_column')
def filter_by_column(tasks, column):
    f = lambda x: x.task_status in settings.TASK_COLUMNS[column]
    return filter(f, tasks)


@register.filter(name='get_features')
def get_features(tasks, developer):
    f = lambda x: x.assigned_to == developer and x.task_type in settings.TASK_FEATURE_TYPES
    return filter(f, tasks)


@register.filter(name='get_enhancements')
def get_enhancements(tasks, developer):
    f = lambda x: x.assigned_to == developer and x.task_type in ('Enhancement',)
    return filter(f, tasks)


@register.filter(name='get_defects')
def get_defects(tasks, developer):
    f = lambda x: x.assigned_to == developer and x.task_type in ('Defect',)
    return filter(f, tasks)


@register.filter(name='get_all')
def get_all(tasks, developer):
    f = lambda x: x.assigned_to == developer
    return filter(f, tasks)


@register.filter(name='lsplit')
def lsplit(value, sep):
    return value.split(sep, 1)[0]


@register.filter(name='rsplit')
def rsplit(value, sep):
    if sep not in value:
        return value
    return value.split(sep, 1)[1]


@register.filter(name='assigned_to')
def assigned_to(value):
    value = value.split('@', 1)[0]
    if hasattr(settings, 'USERNAME_ALIASES') and value in settings.USERNAME_ALIASES:
        return settings.USERNAME_ALIASES[value]
    if '.' in value:
        return value.replace('.', ' ')
    else:
        return value[:1] + ' ' + value[1:]

