from django.views.generic import TemplateView, FormView, RedirectView, CreateView
from django.http import JsonResponse
from django.conf import settings
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import logout, login, authenticate
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from main.models import Task
from main.backends import BzBackend
from main.forms import TaskFilterForm, BacklogFilterForm, ReportFilterForm, LoginForm
from main.helpers import (
    get_and_reset_session,
    get_start_current_week,
    get_current_year,
    get_current_week,
    set_hours
)


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = reverse_lazy('backlog')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            self.request.session['bz_token'] = user._token
            login(self.request, user)
        return super(LoginView, self).form_valid(form)


class LogoutView(RedirectView):
    url = reverse_lazy('login')

    def get_redirect_url(self):
        if self.request.user.username != 'admin':
            BzBackend().logout(self.request.session.get('bz_token'))
        self.request.session['bz_token'] = None
        logout(self.request)
        return super(LogoutView, self).get_redirect_url()

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(LogoutView, self).dispatch(*args, **kwargs)



class FilterTemplateView(TemplateView):
    filter_form_class = None
    session_key = None

    def get_filter_form_kwargs(self):
        return dict(
            data=get_and_reset_session(
                self.request.session,
                self.session_key,
                is_reset=self.request.GET.get('is_reset') is not None
            )
        )

    def get_filter_form(self):
        return self.filter_form_class(**self.get_filter_form_kwargs())

    def get_context_data(self, **kwargs):
        kwargs = super(FilterTemplateView, self).get_context_data(**kwargs)
        if self.request.GET.get('is_submit') is not None:
            self.request.session[self.session_key] = dict(self.request.GET)
        kwargs['filter_form'] = self.get_filter_form()
        kwargs['is_filter'] = self.request.session.get(self.session_key) is not None
        kwargs['bugzilla_url'] = settings.BUGZILLA_URL
        return kwargs


class TaskBoardView(FilterTemplateView):
    template_name = 'board.html'
    filter_form_class = TaskFilterForm
    session_key = 'tasks_filter'

    def get_context_data(self, **kwargs):
        kwargs = super(TaskBoardView, self).get_context_data(**kwargs)

        filter_form = self.get_filter_form()
        if filter_form.is_valid():
            kwargs['tasks'] = Task.objects.get_tasks(
                self.request.bz,
                self.request.session.get('bz_token'),
                **filter_form.cleaned_data
            )
        else:
            kwargs['tasks'] = ()
        kwargs['columns'] = settings.TASK_COLUMNS.keys()
        return kwargs

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(TaskBoardView, self).dispatch(*args, **kwargs)


class BacklogBoardView(FilterTemplateView):
    template_name = 'backlog.html'
    filter_form_class = BacklogFilterForm
    session_key = 'backlog_filter'

    def get_context_data(self, **kwargs):
        kwargs = super(BacklogBoardView, self).get_context_data(**kwargs)

        filter_form = self.get_filter_form()
        if filter_form.is_valid():
            task_kwargs = dict(filter_form.cleaned_data)
            task_kwargs['statuses'] = tuple(set(settings.TASK_STATUSES) - set(settings.TASK_CLOSE_STATUSES) - set([settings.ALL_ITEMS]))
            task_kwargs['types'] = settings.ALL_ITEMS
            task_kwargs['priorities'] = settings.ALL_ITEMS
            task_kwargs['severities'] = settings.ALL_ITEMS
            kwargs['tasks'] = Task.objects.get_tasks(
                self.request.bz,
                self.request.session.get('bz_token'),
                **task_kwargs
            )

            task_kwargs['users'] = filter_form.cleaned_data.get('backlog_users', ())
            task_kwargs['groups'] = ()
            kwargs['backlog_tasks'] = Task.objects.get_tasks(
                self.request.bz,
                self.request.session.get('bz_token'),
                **task_kwargs
            )

            users = filter_form.cleaned_data.get('users', ())
            if settings.ALL_ITEMS in users:
                users = settings.TASK_USERS

            groups = filter_form.cleaned_data.get('groups', ())
            if settings.ALL_ITEMS in groups:
                groups = settings.TASK_GROUPS.keys()

            for group in groups:
                users += settings.TASK_GROUPS[group]

            kwargs['developers'] = sorted(tuple(set(users)))
        else:
            kwargs['tasks'] = ()
            kwargs['backlog_tasks'] = ()
            kwargs['developers'] = ()
        return kwargs

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(BacklogBoardView, self).dispatch(*args, **kwargs)


class ReportView(FilterTemplateView):
    template_name = 'report.html'
    filter_form_class = ReportFilterForm
    session_key = 'report_filter'

    def get_filter_form_kwargs(self):
        kwargs = super(ReportView, self).get_filter_form_kwargs()
        kwargs['initial'] = {'users': [self.request.user.username]}
        return kwargs

    def get_context_data(self, **kwargs):
        kwargs = super(ReportView, self).get_context_data(**kwargs)
        filter_form = self.get_filter_form()
        if filter_form.is_valid():
            task_kwargs = dict(filter_form.cleaned_data)
            task_kwargs['types'] = settings.ALL_ITEMS
            task_kwargs['priorities'] = settings.ALL_ITEMS
            task_kwargs['severities'] = settings.ALL_ITEMS
            task_kwargs['milestones'] = settings.ALL_ITEMS

            task_kwargs['statuses'] = tuple(set(settings.TASK_STATUSES) - set(settings.TASK_CLOSE_STATUSES) - set([settings.ALL_ITEMS]))
            report_tasks = Task.objects.get_tasks(
                self.request.bz,
                self.request.session.get('bz_token'),
                is_load_additional_info=True,
                **task_kwargs
            )

            task_kwargs['last_change_time'] = str(get_start_current_week())
            task_kwargs['statuses'] = settings.TASK_CLOSE_STATUSES
            report_tasks += Task.objects.get_tasks(
                self.request.bz,
                self.request.session.get('bz_token'),
                is_load_additional_info=True,
                **task_kwargs
            )
            report_tasks = filter(lambda x: x.get_u() == u'U', report_tasks)
            set_hours(report_tasks)
            kwargs['report_tasks'] = sorted(report_tasks, key=lambda x: x.assigned_to)
        else:
            kwargs['report_tasks'] = ()
        return kwargs

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(ReportView, self).dispatch(*args, **kwargs)
