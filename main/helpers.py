import datetime

from django.conf import settings


def get_choices(items, handler=lambda x: x):
    return ((item, handler(item).lower()) for item in items)


def short_str(str_data, length, separator=None, item=0):
    if separator and separator in str_data:
        str_data = str_data.split(separator)[item]
    str_end = ''
    if len(str_data) > length:
        str_end = u'...'
    return str_data[:length] + str_end


def get_current_year():
    today = datetime.date.today()
    return today.year


def get_current_week():
    today = datetime.date.today()
    return today.isocalendar()[1]


def get_start_current_week():
    today = datetime.date.today()
    start_week = today - datetime.timedelta(days=today.weekday())
    return start_week


def get_and_reset_session(session, name, is_reset=True):
    data = session.get(name, None)
    if is_reset:
        session[name] = None
        return None
    return data


def get_current_milestone():
    today = datetime.date.today()
    for milestone in settings.TASK_MILESTONES:
        try:
            milestone_date = datetime.datetime.strptime(milestone.split('>')[0], '%m%d%y').date()
            if today <= milestone_date and today > milestone_date - datetime.timedelta(days=28):
                return milestone
        except ValueError:
            pass
    return None


def set_hours(bugs):
    today = datetime.date.today()
    hours = 8 * (today.weekday() + 1)

    hours_bugs = filter(lambda x: x.get_u() == 'U', bugs)

    hours_features = filter(lambda x: x.task_type in settings.TASK_FEATURE_TYPES, hours_bugs)
    hours_enhancements = filter(lambda x: x.task_type in ('Enhancement',), hours_bugs)
    hours_defects = filter(lambda x: x.task_type in ('Defect',), hours_bugs)

    enhancement_coef = 2 if hours_enhancements else 0
    feature_coef = 4 if hours_features else 0

    d = float(len(hours_defects) + enhancement_coef * len(hours_enhancements) + feature_coef * len(hours_features))
    if d == 0:
        return bugs
    d_hours = int(float(hours) / d)
    e_hours = int(float(enhancement_coef * hours) / d)
    f_hours = int(float(feature_coef * hours) / d)

    for bug in hours_defects:
        bug.actual_time = d_hours

    for bug in hours_enhancements:
        bug.actual_time = e_hours

    for bug in hours_features:
        bug.actual_time = f_hours

    rest = hours - (d_hours * len(hours_defects) + e_hours * len(hours_enhancements) + f_hours * len(hours_features))
    if hours_features:
        hours_features[0].actual_time += rest
    elif hours_enhancements:
        hours_enhancements[0].actual_time += rest
    elif hours_defects:
        hours_defects[0].actual_time += rest

    return bugs
