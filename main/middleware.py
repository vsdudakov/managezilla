#-*- encoding: utf-8 -*-
import xmlrpclib

from django.conf import settings


class BzMiddleware(object):

    def process_request(self, request):
        request.bz = xmlrpclib.ServerProxy(settings.BUGZILLA_URL + 'xmlrpc.cgi')